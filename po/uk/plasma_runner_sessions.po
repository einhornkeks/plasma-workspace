# Translation of plasma_runner_sessions.po to Ukrainian
# Copyright (C) 2008-2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2008, 2009, 2010, 2011, 2018, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-26 00:48+0000\n"
"PO-Revision-Date: 2022-10-26 17:04+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: sessionrunner.cpp:28 sessionrunner.cpp:68
#, kde-format
msgctxt "log out command"
msgid "logout"
msgstr "logout"

#: sessionrunner.cpp:28
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Завершує поточний сеанс стільниці"

#: sessionrunner.cpp:29 sessionrunner.cpp:85
#, kde-format
msgctxt "shut down computer command"
msgid "shut down"
msgstr "shut down"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Вимикає комп’ютер"

#: sessionrunner.cpp:33 sessionrunner.cpp:94
#, kde-format
msgctxt "lock screen command"
msgid "lock"
msgstr "lock"

#: sessionrunner.cpp:33
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Заблоковує поточні сеанси і запускає зберігач екрана"

#: sessionrunner.cpp:36 sessionrunner.cpp:76
#, kde-format
msgctxt "restart computer command"
msgid "restart"
msgstr "restart"

#: sessionrunner.cpp:36
#, kde-format
msgid "Reboots the computer"
msgstr "Перезавантажує комп’ютер"

#: sessionrunner.cpp:37 sessionrunner.cpp:77
#, kde-format
msgctxt "restart computer command"
msgid "reboot"
msgstr "reboot"

#: sessionrunner.cpp:40 sessionrunner.cpp:104
#, kde-format
msgctxt "save session command"
msgid "save"
msgstr "save"

#: sessionrunner.cpp:40
#, kde-format
msgid "Saves the current session for session restoration"
msgstr "Зберігає поточний сеанс для відновлення згодом"

#: sessionrunner.cpp:41 sessionrunner.cpp:105
#, kde-format
msgctxt "save session command"
msgid "save session"
msgstr "save session"

#: sessionrunner.cpp:44
#, kde-format
msgctxt "switch user command"
msgid "switch"
msgstr "switch"

#: sessionrunner.cpp:45
#, kde-format
msgctxt "switch user command"
msgid "switch :q:"
msgstr "switch :q:"

#: sessionrunner.cpp:46
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Перемикає активний сеанс для користувача :q: або показує список всіх "
"активних сеансів, якщо :q: не вказано"

#: sessionrunner.cpp:49 sessionrunner.cpp:144
#, kde-format
msgid "switch user"
msgstr "перемкнути користувача"

#: sessionrunner.cpp:49
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Запускає новий сеанс від імені іншого користувача"

#: sessionrunner.cpp:50 sessionrunner.cpp:144
#, kde-format
msgid "new session"
msgstr "новий сеанс"

#: sessionrunner.cpp:54
#, kde-format
msgid "Lists all sessions"
msgstr "Список усіх сеансів"

#: sessionrunner.cpp:68
#, kde-format
msgid "log out"
msgstr "вийти"

#: sessionrunner.cpp:70
#, kde-format
msgctxt "log out command"
msgid "Logout"
msgstr "Вийти"

#: sessionrunner.cpp:79
#, kde-format
msgid "Restart the computer"
msgstr "Перезапустити комп’ютер"

#: sessionrunner.cpp:86
#, kde-format
msgctxt "shut down computer command"
msgid "shutdown"
msgstr "shutdown"

#: sessionrunner.cpp:88
#, kde-format
msgid "Shut down the computer"
msgstr "Вимкнути комп’ютер"

#: sessionrunner.cpp:97
#, kde-format
msgid "Lock the screen"
msgstr "Заблокувати екран"

#: sessionrunner.cpp:107
#, kde-format
msgid "Save the session"
msgstr "Зберегти сеанс"

#: sessionrunner.cpp:108
#, kde-format
msgid "Save the current session for session restoration"
msgstr "Зберегти поточний сеанс для відновлення згодом"

#: sessionrunner.cpp:129
#, kde-format
msgctxt "User sessions"
msgid "sessions"
msgstr "сеанси"

#: sessionrunner.cpp:150
#, kde-format
msgid "Switch User"
msgstr "Змінити користувача"

#: sessionrunner.cpp:227
#, kde-format
msgid "New Session"
msgstr "Новий сеанс"

#: sessionrunner.cpp:228
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type 'switch' or 'sessions')</li><li>Plasma widgets "
"(such as the application launcher)</li></ul>"
msgstr ""
"<p>Ви на порозі входу до нового стільничного сеансу.</p><p>Буде показано "
"вікно вітання, а поточний сеанс буде приховано.</p><p>Перемикатися між "
"стільничними сеансами можна за допомогою таких засобів:</p><ul><li>Ctrl+Alt"
"+F{номер сеансу}</li><li>Пошуку Плазми (введіть «switch» або «sessions»)</"
"li><li>Віджетів Плазми (зокрема віджета запуску програм)</li></ul>"

#~ msgid "Warning - New Session"
#~ msgstr "Увага — новий сеанс"

#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Ви забажали відкрити новий стільничний сеанс.<br />Поточний сеанс буде "
#~ "сховано, і ви побачите нове вікно входу до системи.<br />Кожному сеансу "
#~ "відповідає F-клавіша; F%1, зазвичай, відповідає першому сеансу, F%2 — "
#~ "другому і так далі. Ви можете перемикатися між сеансами одночасним "
#~ "натисканням клавіш Ctrl, Alt і відповідної F-клавіші. Крім того, у меню "
#~ "панелі і стільниці Плазми є пункти для перемикання між сеансами.</p>"
