# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2014-10-29 11:51+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr ""

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr ""

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:431
#, kde-format
msgid "Time Zones"
msgstr ""

#: package/contents/ui/CalendarView.qml:110
#, kde-format
msgid "Events"
msgstr ""

#: package/contents/ui/CalendarView.qml:118
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr ""

#: package/contents/ui/CalendarView.qml:122
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr ""

#: package/contents/ui/CalendarView.qml:397
#, kde-format
msgid "No events for today"
msgstr ""

#: package/contents/ui/CalendarView.qml:398
#, kde-format
msgid "No events for this day"
msgstr ""

#: package/contents/ui/CalendarView.qml:440
#, kde-format
msgid "Switch…"
msgstr ""

#: package/contents/ui/CalendarView.qml:441
#: package/contents/ui/CalendarView.qml:444
#, kde-format
msgid "Switch to another timezone"
msgstr ""

#: package/contents/ui/configAppearance.qml:51
#, kde-format
msgid "Information:"
msgstr ""

#: package/contents/ui/configAppearance.qml:55
#, kde-format
msgid "Show date"
msgstr ""

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "Adaptive location"
msgstr ""

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "Always beside time"
msgstr ""

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "Always below time"
msgstr ""

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgid "Show seconds"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "Show time zone:"
msgstr ""

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "Only when different from local time zone"
msgstr ""

#: package/contents/ui/configAppearance.qml:91
#, kde-format
msgid "Always"
msgstr ""

#: package/contents/ui/configAppearance.qml:100
#, kde-format
msgid "Display time zone as:"
msgstr ""

#: package/contents/ui/configAppearance.qml:105
#, kde-format
msgid "Code"
msgstr ""

#: package/contents/ui/configAppearance.qml:106
#, kde-format
msgid "City"
msgstr ""

#: package/contents/ui/configAppearance.qml:107
#, kde-format
msgid "Offset from UTC time"
msgstr ""

#: package/contents/ui/configAppearance.qml:119
#, kde-format
msgid "Time display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:124
#, kde-format
msgid "12-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:125
#: package/contents/ui/configCalendar.qml:58
#, kde-format
msgid "Use Region Defaults"
msgstr ""

#: package/contents/ui/configAppearance.qml:126
#, kde-format
msgid "24-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:133
#, kde-format
msgid "Change Regional Settings…"
msgstr ""

#: package/contents/ui/configAppearance.qml:144
#, kde-format
msgid "Date format:"
msgstr ""

#: package/contents/ui/configAppearance.qml:152
#, kde-format
msgid "Long Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:157
#, kde-format
msgid "Short Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:162
#, kde-format
msgid "ISO Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:167
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr ""

#: package/contents/ui/configAppearance.qml:198
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-5/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""

#: package/contents/ui/configAppearance.qml:222
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:224
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr ""

#: package/contents/ui/configAppearance.qml:228
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""

#: package/contents/ui/configAppearance.qml:237
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr ""

#: package/contents/ui/configAppearance.qml:247
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr ""

#: package/contents/ui/configAppearance.qml:260
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr ""

#: package/contents/ui/configAppearance.qml:271
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr ""

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "General:"
msgstr ""

#: package/contents/ui/configCalendar.qml:45
#, kde-format
msgid "Show week numbers"
msgstr ""

#: package/contents/ui/configCalendar.qml:50
#, kde-format
msgid "First day of week:"
msgstr ""

#: package/contents/ui/configCalendar.qml:73
#, kde-format
msgid "Available Plugins:"
msgstr ""

#: package/contents/ui/configTimeZones.qml:90
#, kde-format
msgid "Clock is currently using this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:100
#, kde-format
msgid "Switch Local Time Zone…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:109
#, kde-format
msgid "Remove this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:118
#, kde-format
msgid "System's Local Time Zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:118
#, kde-format
msgid "Additional Time Zones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:131
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""

#: package/contents/ui/configTimeZones.qml:138
#, kde-format
msgid "Add Time Zones…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:149
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr ""

#: package/contents/ui/configTimeZones.qml:160
#, kde-format
msgid ""
"Note that using a different time zone for the clock does not change the "
"systemwide local time zone. When you travel, switch the local time zone "
"instead."
msgstr ""

#: package/contents/ui/configTimeZones.qml:189
#, kde-format
msgid "Add More Timezones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:200
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""

#: package/contents/ui/configTimeZones.qml:225
#, kde-format
msgid "%1, %2 (%3)"
msgstr ""

#: package/contents/ui/configTimeZones.qml:225
#, kde-format
msgid "%1, %2"
msgstr ""

#: package/contents/ui/main.qml:128
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Adjust Date and Time…"
msgstr ""

#: package/contents/ui/main.qml:136
#, kde-format
msgid "Set Time Format…"
msgstr ""

#: package/contents/ui/Tooltip.qml:31
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr ""

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr ""

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr ""

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr ""

#: plugin/timezonemodel.cpp:141
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr ""

#: plugin/timezonemodel.cpp:143
#, kde-format
msgid "System's local time zone"
msgstr ""

#, fuzzy
#~| msgctxt ""
#~| "This composes time and a timezone into one string that's displayed in "
#~| "the tooltip of the clock. "
#~| msgid "%1 %2"
#~ msgctxt "Format: month year"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#~ msgctxt ""
#~ "This composes time and a timezone into one string that's displayed in the "
#~ "clock applet (the main clock in the panel). "
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgctxt ""
#~ "This composes time and a timezone into one string that's displayed in the "
#~ "clock applet (the main clock in the panel). "
#~ msgid "%1<br/>%2"
#~ msgstr "%1<br/>%2"
