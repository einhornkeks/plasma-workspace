# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Paolo Zamponi <zapaolo@email.it>, 2019, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-26 00:47+0000\n"
"PO-Revision-Date: 2022-07-24 15:49+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: job_p.cpp:185
#, kde-format
msgctxt "Copying n of m files to locaton"
msgid "%2 of %1 file to %3"
msgid_plural "%2 of %1 files to %3"
msgstr[0] "%2 di %1 file in %3"
msgstr[1] "%2 di %1 file in %3"

#: job_p.cpp:188 job_p.cpp:218 job_p.cpp:234
#, kde-format
msgctxt "Copying n files to location"
msgid "%1 file to %2"
msgid_plural "%1 files to %2"
msgstr[0] "%1 file in %2"
msgstr[1] "%1 file in %2"

#: job_p.cpp:195
#, kde-format
msgctxt "Copying n of m files"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] "%2 file di %1"
msgstr[1] "%2 file di %1"

#: job_p.cpp:198 job_p.cpp:228 job_p.cpp:238
#, kde-format
msgctxt "Copying n files"
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] "%1 file"
msgstr[1] "%1 file"

#: job_p.cpp:203
#, kde-format
msgctxt "Copying n of m items"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] "%2 di %1 elemento"
msgstr[1] "%2 di %1 elementi"

#: job_p.cpp:206 job_p.cpp:226
#, kde-format
msgctxt "Copying n items"
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "%1 elemento"
msgstr[1] "%1 elementi"

#: job_p.cpp:213
#, kde-format
msgctxt "Copying file to location"
msgid "%1 to %2"
msgstr "%1 in %2"

#: job_p.cpp:216
#, kde-format
msgctxt "Copying n items to location"
msgid "%1 file to %2"
msgid_plural "%1 items to %2"
msgstr[0] "%1 file in %2"
msgstr[1] "%1 file in %2"

#: job_p.cpp:236
#, kde-format
msgctxt "Copying unknown amount of files to location"
msgid "to %1"
msgstr "in %1"

#: jobsmodel_p.cpp:476
#, kde-format
msgid "Application closed unexpectedly."
msgstr "L'applicazione si è chiusa in maniera inaspettata."
