# Frederik Schwarzer <schwarzer@kde.org>, 2009, 2014, 2022.
# Thomas Reitelbach <tr@erdfunkstelle.de>, 2009.
# Burkhard Lück <lueck@hube-lueck.de>, 2014, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_containmentactions_contextmenu\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-11 00:47+0000\n"
"PO-Revision-Date: 2022-02-26 02:44+0100\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-doc@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.0\n"

#: menu.cpp:99
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "KRunner anzeigen"

#: menu.cpp:104
#, kde-format
msgid "Open Terminal"
msgstr ""

#: menu.cpp:108
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Bildschirm sperren"

#: menu.cpp:117
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Verlassen ..."

#: menu.cpp:126
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr "Anzeige einrichten ..."

#: menu.cpp:291
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Modul für Kontextmenü einrichten"

#: menu.cpp:301
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[Andere Aktionen]"

#: menu.cpp:304
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Hintergrundbild-Aktionen"

#: menu.cpp:308
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[Trenner]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "Befehl ausführen ..."

#~ msgid "Add Panel"
#~ msgstr "Kontrollleiste hinzufügen"
