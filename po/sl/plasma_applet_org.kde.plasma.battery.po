# translation of plasma_applet_battery.po to Slovenian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
#
# Andrej Vernekar <andrej.vernekar@moj.net>, 2007, 2011, 2012.
# Jure Repinc <jlp@holodeck1.com>, 2007, 2008, 2009, 2010, 2011, 2012.
# Andrej Mernik <andrejm@ubuntu.si>, 2013, 2014, 2015, 2016, 2017.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
# Martin Srebotnjak <miles@filmsi.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-05 00:47+0000\n"
"PO-Revision-Date: 2022-11-05 09:06+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Poedit 3.1.1\n"

#: package/contents/ui/BatteryItem.qml:106
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:167
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Zmogljivost te baterije je samo %1% in jo je potrebno zamenjati. Obrnite se "
"na proizvajalca računalnika."

#: package/contents/ui/BatteryItem.qml:182
#, kde-format
msgid "Time To Full:"
msgstr "Čas do napolnjenja:"

#: package/contents/ui/BatteryItem.qml:183
#, kde-format
msgid "Remaining Time:"
msgstr "Preostali čas:"

#: package/contents/ui/BatteryItem.qml:199
#, kde-format
msgid "Battery Health:"
msgstr "Zdravje baterije:"

#: package/contents/ui/BatteryItem.qml:205
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:219
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Vaša baterija je nastavljena za polnjenje do približno %1%."

#: package/contents/ui/BrightnessItem.qml:64
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/CompactRepresentation.qml:102
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:12
#, kde-format
msgid "Discharging"
msgstr "Praznjenje"

#: package/contents/ui/logic.js:13 package/contents/ui/main.qml:144
#, kde-format
msgid "Fully Charged"
msgstr "Povsem polna"

#: package/contents/ui/logic.js:14
#, kde-format
msgid "Charging"
msgstr "Polnjenje"

#: package/contents/ui/logic.js:16
#, kde-format
msgid "Not Charging"
msgstr "Se ne polni"

#: package/contents/ui/logic.js:19
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Ni prisotna"

#: package/contents/ui/main.qml:103 package/contents/ui/main.qml:350
#, kde-format
msgid "Battery and Brightness"
msgstr "Baterija in svetlost"

#: package/contents/ui/main.qml:104
#, kde-format
msgid "Brightness"
msgstr "Svetlost"

#: package/contents/ui/main.qml:105
#, kde-format
msgid "Battery"
msgstr "Baterija"

#: package/contents/ui/main.qml:105
#, kde-format
msgid "Power Management"
msgstr "Upravljanje porabe"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Baterija na %1%, se ne polni"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Baterija na %1%, je priključena, a se še vedno prazni"

#: package/contents/ui/main.qml:155
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Baterija na %1%, se polni"

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Battery at %1%"
msgstr "Baterija na %1%"

#: package/contents/ui/main.qml:166
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Napajalnik ni dovolj zmogljiv, da bi polnil baterijo"

#: package/contents/ui/main.qml:170
#, kde-format
msgid "No Batteries Available"
msgstr "Baterija ni na voljo"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 do polne napolnjenosti"

#: package/contents/ui/main.qml:178
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 preostalo"

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Not charging"
msgstr "Se ne polni"

#: package/contents/ui/main.qml:185
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "Samodejno spanje in zaklepanje zaslona sta onemogočena"

#: package/contents/ui/main.qml:190
#, kde-format
msgid "Performance mode has been manually enabled"
msgstr "Učinkoviti način je bil ročno omogočen"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "%1 aplikacija je zahtevala aktiviranje učinkovitega načina"
msgstr[1] "%1 aplikaciji sta zahtevali aktiviranje učinkovitega načina"
msgstr[2] "%1 aplikacije so zahtevale aktiviranje učinkovitega načina"
msgstr[3] "%1 aplikacij je zahtevalo aktiviranje učinkovitega načina"

#: package/contents/ui/main.qml:197
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "%1 aplikacija je zahtevala aktiviranje varčevalnega načina"
msgstr[1] "%1 aplikaciji sta zahtevali aktiviranje varčevalnega načina"
msgstr[2] "%1 aplikacije so zahtevale aktiviranje varčevalnega načina"
msgstr[3] "%1 aplikacij je zahtevalo aktiviranje varčevalnega načina"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Aplet za baterijo je omogočil sistemsko inhibicijo"

#: package/contents/ui/main.qml:353
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Aktiviranje načina %1 ni uspelo"

#: package/contents/ui/main.qml:365
#, kde-format
msgid "&Show Energy Information…"
msgstr "Prikaži informacije o energiji…"

#: package/contents/ui/main.qml:367
#, kde-format
msgid "Show Battery Percentage on Icon"
msgstr "Pokaži odstotek baterije na ikoni"

#: package/contents/ui/main.qml:373
#, kde-format
msgid "&Configure Energy Saving…"
msgstr "Nastavi varčevanje z energijo…"

#: package/contents/ui/PopupDialog.qml:117
#, kde-format
msgid "Display Brightness"
msgstr "Svetlost zaslona"

#: package/contents/ui/PopupDialog.qml:146
#, kde-format
msgid "Keyboard Brightness"
msgstr "Svetlost tipkovnice"

#: package/contents/ui/PowerManagementItem.qml:37
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Ročno preprečuj spanje in zaklepanje zaslona"

#: package/contents/ui/PowerManagementItem.qml:70
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Vaš prenosnik je nastavljen, da ne preide v stanje spanja ob zaprtju "
"pokrova, ko je nanj povezan zunanji zaslon."

#: package/contents/ui/PowerManagementItem.qml:83
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "%1 program trenutno preprečuje spanje in zaklepanje zaslona:"
msgstr[1] "%1 programa trenutno preprečujeta spanje in zaklepanje zaslona:"
msgstr[2] "%1 programi trenutno preprečujejo spanje in zaklepanje zaslona:"
msgstr[3] "%1 programov trenutno preprečuje spanje in zaklepanje zaslona:"

#: package/contents/ui/PowerManagementItem.qml:103
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 trenutno preprečuje spanje in zaklepanje zaslona (%2)"

#: package/contents/ui/PowerManagementItem.qml:104
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 trenutno preprečuje spanje in zaklepanje zaslona (neznan razlog)"

#: package/contents/ui/PowerManagementItem.qml:106
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: neznan vzrok"

#: package/contents/ui/PowerProfileItem.qml:34
#, kde-format
msgid "Power Save"
msgstr "Ohranjanje energije"

#: package/contents/ui/PowerProfileItem.qml:38
#, kde-format
msgid "Balanced"
msgstr "Uravnoteženo"

#: package/contents/ui/PowerProfileItem.qml:42
#, kde-format
msgid "Performance"
msgstr "Učinkovitost"

#: package/contents/ui/PowerProfileItem.qml:59
#, kde-format
msgid "Power Profile"
msgstr "Profil upravljanja z energijo"

#: package/contents/ui/PowerProfileItem.qml:191
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Učinkovitost je bila zmanjšana, da bi se zmanjšalo gretje, ker je računalnik "
"ugotovil, da se nahaja v vašem naročju."

#: package/contents/ui/PowerProfileItem.qml:193
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Učinkovit način ni na voljo, ker je računalnik že preveč ogret."

#: package/contents/ui/PowerProfileItem.qml:195
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Učinkovit način ni na voljo."

#: package/contents/ui/PowerProfileItem.qml:208
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Učinkovitost je lahko zmanjšana, da bi se zmanjšalo gretje, ker je "
"računalnik ugotovil, da se nahaja v vašem naročju."

#: package/contents/ui/PowerProfileItem.qml:210
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Učinkovitost je lahko zmanjšana, ker je računalnik preveč ogret."

#: package/contents/ui/PowerProfileItem.qml:212
#, kde-format
msgid "Performance may be reduced."
msgstr "Učinkovitost je lahko zmanjšana."

#: package/contents/ui/PowerProfileItem.qml:223
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "%1 aplikacija je zahtevala aktiviranje %2:"
msgstr[1] "%1 aplikaciji sta zahtevali aktiviranje %2:"
msgstr[2] "%1 aplikacije so zahtevale aktiviranje %2:"
msgstr[3] "%1 aplikacij je zahtevalo aktiviranje %2:"

#: package/contents/ui/PowerProfileItem.qml:241
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"
