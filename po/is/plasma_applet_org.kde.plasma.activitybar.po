# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2022-08-25 10:07+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/main.qml:70
#, kde-format
msgid "Switch to activity %1"
msgstr "Skipta yfir í \"%1\" athöfnina"

#: contents/ui/main.qml:99
#, kde-format
msgctxt "@action:inmenu"
msgid "&Configure Activities…"
msgstr "Stilla At&hafnir…"
