# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2013, 2014, 2015, 2016, 2017, 2019, 2020.
# Vit Pelcak <vpelcak@suse.cz>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-28 00:47+0000\n"
"PO-Revision-Date: 2022-06-29 12:15+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.2\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Obecné"

#: package/contents/config/config.qml:18
#, kde-format
msgid "Entries"
msgstr "Položky"

#: package/contents/ui/ConfigEntries.qml:31
#, kde-format
msgid "Always show all entries"
msgstr "Vždy zobrazit všechny položky"

#: package/contents/ui/ConfigEntries.qml:37
#, kde-format
msgid "Application Status"
msgstr "Stav aplikace"

#: package/contents/ui/ConfigEntries.qml:39
#, kde-format
msgid "Communications"
msgstr "Komunikace"

#: package/contents/ui/ConfigEntries.qml:41
#, kde-format
msgid "System Services"
msgstr "Systémové služby"

#: package/contents/ui/ConfigEntries.qml:43
#, kde-format
msgid "Hardware Control"
msgstr "Ovládání hardware"

#: package/contents/ui/ConfigEntries.qml:46
#, kde-format
msgid "Miscellaneous"
msgstr "Různé"

#: package/contents/ui/ConfigEntries.qml:80
#, kde-format
msgctxt "Name of the system tray entry"
msgid "Entry"
msgstr "Záznam"

#: package/contents/ui/ConfigEntries.qml:85
#, kde-format
msgid "Visibility"
msgstr "Viditelnost"

#: package/contents/ui/ConfigEntries.qml:91
#, kde-format
msgid "Keyboard Shortcut"
msgstr "Klávesová zkratka"

#: package/contents/ui/ConfigEntries.qml:229
#, kde-format
msgid "Shown when relevant"
msgstr "Zobrazit pokud relevantní"

#: package/contents/ui/ConfigEntries.qml:230
#, kde-format
msgid "Always shown"
msgstr "Vždy zobrazovat"

#: package/contents/ui/ConfigEntries.qml:231
#, kde-format
msgid "Always hidden"
msgstr "Vždy skrytý"

#: package/contents/ui/ConfigEntries.qml:232
#, kde-format
msgid "Disabled"
msgstr "Zakázáno"

#: package/contents/ui/ConfigGeneral.qml:23
#, kde-format
msgctxt "The arrangement of system tray icons in the Panel"
msgid "Panel icon size:"
msgstr "Velikost ikon panelu:"

#: package/contents/ui/ConfigGeneral.qml:25
#, kde-format
msgid "Small"
msgstr "Malý"

#: package/contents/ui/ConfigGeneral.qml:32
#, kde-format
msgid "Scale with Panel height"
msgstr "Měnit s výškou panelu"

#: package/contents/ui/ConfigGeneral.qml:33
#, kde-format
msgid "Scale with Panel width"
msgstr "Měnit s šířkou panelu"

#: package/contents/ui/ConfigGeneral.qml:39
#, kde-format
msgid "Automatically enabled when in Touch Mode"
msgstr "Automaticky povoleno v dotykovém režimu"

#: package/contents/ui/ConfigGeneral.qml:48
#, kde-format
msgctxt "@label:listbox The spacing between system tray icons in the Panel"
msgid "Panel icon spacing:"
msgstr "Rozestupy ikon panelu:"

#: package/contents/ui/ConfigGeneral.qml:51
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Small"
msgstr "Malé"

#: package/contents/ui/ConfigGeneral.qml:55
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Normal"
msgstr "Normální"

#: package/contents/ui/ConfigGeneral.qml:59
#, kde-format
msgctxt "@item:inlistbox Icon spacing"
msgid "Large"
msgstr "Velké"

#: package/contents/ui/ConfigGeneral.qml:82
#, kde-format
msgctxt "@info:usagetip under a combobox when Touch Mode is on"
msgid "Automatically set to Large when in Touch Mode"
msgstr "Automaticky přepnout na velké v dotykovém režimu"

#: package/contents/ui/ExpandedRepresentation.qml:64
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr ""

#: package/contents/ui/ExpandedRepresentation.qml:77
#, kde-format
msgid "Status and Notifications"
msgstr "Stav a upozornění"

#: package/contents/ui/ExpandedRepresentation.qml:93
#, kde-format
msgid "More actions"
msgstr "Další činnosti"

#: package/contents/ui/ExpandedRepresentation.qml:182
#, kde-format
msgid "Keep Open"
msgstr "Ponechat otevřené"

#: package/contents/ui/ExpanderArrow.qml:22
#, kde-format
msgid "Expand System Tray"
msgstr "Rozšířit systémovou část panelu"

#: package/contents/ui/ExpanderArrow.qml:23
#, kde-format
msgid "Show all the items in the system tray in a popup"
msgstr "Zobrazit všechny položky v systémové části panelu"

#: package/contents/ui/ExpanderArrow.qml:36
#, kde-format
msgid "Close popup"
msgstr "Zavřít vyskakovací okno"

#: package/contents/ui/ExpanderArrow.qml:36
#, kde-format
msgid "Show hidden icons"
msgstr "Zobrazit skryté ikony"

#: systemtraymodel.cpp:123
#, kde-format
msgctxt ""
"Suffix added to the applet name if the applet is autoloaded via DBus "
"activation"
msgid "%1 (Automatic load)"
msgstr "%1 (automatické načtení)"
