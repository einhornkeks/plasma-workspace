# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# giovanni <g.sora@tiscali.it>, 2017, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2022-07-15 12:18+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/config/config.qml:12
#, kde-format
msgid "General"
msgstr "General"

#: contents/ui/AlbumArtStackView.qml:161
#, kde-format
msgid "No title"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161 contents/ui/main.qml:80
#, kde-format
msgid "No media playing"
msgstr "Necun media reproducente"

#: contents/ui/ConfigGeneral.qml:22
#, kde-format
msgid "Volume step:"
msgstr "Passo de volumine:"

#: contents/ui/ExpandedRepresentation.qml:361
#: contents/ui/ExpandedRepresentation.qml:482
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: contents/ui/ExpandedRepresentation.qml:511
#, kde-format
msgid "Shuffle"
msgstr "Shuffle (miscer)"

#: contents/ui/ExpandedRepresentation.qml:537 contents/ui/main.qml:98
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Previe tracia"

#: contents/ui/ExpandedRepresentation.qml:559 contents/ui/main.qml:106
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Pausa"

#: contents/ui/ExpandedRepresentation.qml:559 contents/ui/main.qml:111
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Reproduce"

#: contents/ui/ExpandedRepresentation.qml:577 contents/ui/main.qml:117
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Proxime tracia"

#: contents/ui/ExpandedRepresentation.qml:600
#, kde-format
msgid "Repeat Track"
msgstr "Repete tracia"

#: contents/ui/ExpandedRepresentation.qml:600
#, kde-format
msgid "Repeat"
msgstr "Repete"

#: contents/ui/main.qml:94
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Aperi"

#: contents/ui/main.qml:123
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Stoppa"

#: contents/ui/main.qml:131
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Quita"

#: contents/ui/main.qml:241
#, kde-format
msgid "Choose player automatically"
msgstr "Selige reproductor automaticamente"

#: contents/ui/main.qml:278
#, kde-format
msgctxt "by Artist (player name)"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause"
msgstr ""

#: contents/ui/main.qml:278
#, kde-format
msgid ""
"%1\n"
"Middle-click to pause"
msgstr ""

#: contents/ui/main.qml:289
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "by Artist (paused, player name)"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play"
msgstr "per %1 (in pausa, %2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt ""
"Paused (player name)\n"
"Middle-click to play"
msgid ""
"Paused (%1)\n"
"Middle-click to play"
msgstr ""

#~ msgctxt "by Artist (player name)"
#~ msgid "by %1 (%2)"
#~ msgstr "per %1 (%2)"

#~ msgctxt "Paused (player name)"
#~ msgid "Paused (%1)"
#~ msgstr "In pausa (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "per %1"
